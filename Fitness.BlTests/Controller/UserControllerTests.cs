﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fitness.Bl.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fitness.Bl.Model;

namespace Fitness.Bl.Controller.Tests
{
    [TestClass()]
    public class UserControllerTests
    {

        [TestMethod()]
        public void SetNewUserDataTest()
        {
            var userName = Guid.NewGuid().ToString();

            var controller = new UserController(userName);
            controller.SetNewUserData("male",DateTime.Parse("01.01.1990"));
            var controller2=new UserController(userName);

            Assert.AreEqual(controller2.CurrentUser.Gender.Name, controller.CurrentUser.Gender.Name);
        }

        [TestMethod()]
        public void SaveTest()
        {
            var userName = Guid.NewGuid().ToString();

            var controller=new UserController(userName);

            Assert.AreEqual(userName,controller.CurrentUser.Name);
        }
    }
}