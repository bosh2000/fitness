﻿using System;

namespace Fitness.Bl.Model
{
    /// <summary>
    ///     Пользователь.
    /// </summary>
    [Serializable]
    public class User
    {
        /// <summary>
        ///     Создать нового пользователя
        /// </summary>
        /// <param name="name">Имя.</param>
        /// <param name="gender">Пол.</param>
        /// <param name="birthDate">Дата рождения.</param>
        /// <param name="weigth">Вес.</param>
        /// <param name="heigth">Рост.</param>
        public User(string name,
            Gender gender,
            DateTime birthDate,
            double weigth,
            double heigth)
        {
            #region Проверка условий

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("Имя пользователя не может быть пустым.", nameof(name));

            if (gender == null) throw new ArgumentNullException("Пол не может быть null.", nameof(gender));

            if (birthDate <= DateTime.Parse("01.01.1900") || birthDate >= DateTime.Now)
                throw new ArgumentException("Дата не может быть меньше 01.01.1900 или больше текущей даты.",
                    nameof(birthDate));

            if (weigth <= 0)
                throw new ArgumentException("Пол не может быть отрицательным или равным нулю.", nameof(weigth));

            if (heigth <= 0)
                throw new ArgumentException("Рост не может быть отрицательным или равным нулю.", nameof(heigth));

            #endregion Проверка условий

            BirthDate = birthDate;
            Weigth = weigth;
            Heigth = heigth;
            Name = name;
            Gender = gender;
        }

        public User(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("Имя пользователя не может быть пустым.", nameof(name));
            Name = name;
        }

        public override string ToString()
        {
            return $"Name={Name} Gender={Gender.Name} Age={Age} Weigth={Weigth} Heigth={Heigth}";
        }

        #region Свойства.

        /// <summary>
        ///     Имя.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Пол.
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        ///     Дата рождения.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        ///     Вес.
        /// </summary>
        public double Weigth { get; set; }

        /// <summary>
        ///     Рост.
        /// </summary>
        public double Heigth { get; set; }

        public int Age => DateTime.Now.Year - BirthDate.Year;

        #endregion Свойства.
    }
}