﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Fitness.Bl.Model;

namespace Fitness.Bl.Controller
{
    /// <summary>
    ///     Контроллер пользователя.
    /// </summary>
    public class UserController
    {
        /// <summary>
        ///     Создание нового контроллера пользователя.
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        public UserController(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException("Пустое имя пользователя", nameof(userName));
            Users = GetUserData();
            CurrentUser = Users.SingleOrDefault(u => u.Name == userName);
            if (CurrentUser == null)
            {
                CurrentUser = new User(userName);
                Users.Add(CurrentUser);
                IsNewUser = true;
                Save();
            }
        }

        /// <summary>
        ///     Список пользователь приложения.
        /// </summary>
        public List<User> Users { get; }

        /// <summary>
        ///     Текущий пользователь.
        /// </summary>
        public User CurrentUser { get; }

        /// <summary>
        ///     Является ли текущий пользователь новым.
        /// </summary>
        public bool IsNewUser { get; }

        /// <summary>
        ///     Заполнение полей у User.
        /// </summary>
        /// <param name="genderName"></param>
        /// <param name="birthDate"></param>
        /// <param name="weigth"></param>
        /// <param name="heigth"></param>
        public void SetNewUserData(string genderName, DateTime birthDate, double weigth = 1, double heigth = 1)
        {
            //TODO Проверка.

            CurrentUser.Gender = new Gender(genderName);
            CurrentUser.BirthDate = birthDate;
            CurrentUser.Weigth = weigth;
            CurrentUser.Heigth = heigth;
            Save();
        }

        /// <summary>
        ///     Получить сохраненный список пользователей
        /// </summary>
        /// <returns></returns>
        private List<User> GetUserData()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("users.dat", FileMode.OpenOrCreate))
            {
                try
                {
                    if (formatter.Deserialize(fs) is List<User> users)
                        return users;
                }
                catch
                {
                }
            }

            return new List<User>();
        }

        /// <summary>
        ///     Сохранение данных пользователя.
        /// </summary>
        public void Save()
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream("users.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, Users);
            }
        }
    }
}