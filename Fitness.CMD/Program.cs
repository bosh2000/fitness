﻿using System;
using Fitness.Bl.Controller;

namespace Fitness.CMD
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Вас приветствует приложение Fitness");
            Console.WriteLine("Введите имя пользователя - ");

            var name = "Vasya";

            var userController = new UserController(name);
            if (userController.IsNewUser)
            {
                var gender = "male";
                var birthDate = DateTime.Parse("01.01.1989");
                var weigth = 100;
                var heigth = 180;
                userController.SetNewUserData(gender, birthDate, weigth, heigth);
            }

            Console.WriteLine(userController.CurrentUser);
            Console.WriteLine("End. Press any key");
            Console.ReadLine();
            //do
            //{
            //    var str = Console.ReadLine();
            //    if (str.ToUpper().Equals("EXIT"))
            //    {
            //        break;
            //    }

            //    var user=new UserController(str);
            //} while (true);
        }
    }
}